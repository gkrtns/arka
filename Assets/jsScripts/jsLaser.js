private var speed = 8.0;

function Update () {
	var amtMove = speed * Time.deltaTime;
	transform.Translate(Vector3.forward * amtMove);
	if (transform.position.z > 2)
		Destroy(gameObject);
}

function OnTriggerEnter(coll : Collider) {
	if (coll.gameObject.tag.Substring(0, 5) == "BLOCK") {
		coll.gameObject.SendMessage("SetCollision", true, SendMessageOptions.DontRequireReceiver);
	}
	Destroy(gameObject);
}	

