var sndLaser : AudioClip;		//레이저 발사
var laser  	: Transform;

static var laserTime = 0.0;

private var speed = 10;
private var ball : GameObject;
private var arAngles = [-30, -45, -60, 60, 45, 30];

function Start() {
	ball = GameObject.Find("공");
}

function Update () {
	var limit = 3.8 - transform.localScale.x / 2;
	
	if (jsGameManager.state == STATE.DEMO) {
		transform.position.x = ball.transform.position.x;
		transform.position.x = Mathf.Clamp(transform.position.x, -limit, limit);
		return;
	}

	var key = Input.GetAxis("Horizontal");
	var amtToMove = speed * Time.deltaTime * key;
	
	transform.Translate(Vector3.right * amtToMove);	
	transform.position.x = Mathf.Clamp(transform.position.x, -limit, limit);

	if (jsGameManager.state == STATE.READY) {
		ball.transform.position.x = transform.position.x;

		if (Input.GetButtonDown("Fire1")) {
			jsGameManager.state = STATE.IDLE;
		}
	}
	
	laserTime -= Time.deltaTime;
	if (laserTime > 0) {
		FireLaser();
	}
}

function OnTriggerEnter(coll : Collider) {
	if (coll.gameObject.tag.Substring(0, 4) == "BALL") {
		audio.Play();
		
		var n = Random.Range(0, 6);
		coll.transform.rotation.eulerAngles.y = arAngles[n];
		return;
	}
	if (coll.gameObject.tag.Substring(0, 5) == "BONUS") {
		jsGameManager.bonusNum = int.Parse(coll.tag.Substring(5, 1));
		Destroy(coll.transform.root.gameObject);
		jsGameManager.state = STATE.BONUS;
	}	
}
function FireLaser() {
	if (Input.GetButtonDown("Fire1")) {
		var pos = transform.position;
		Instantiate(laser, Vector3(pos.x -0.25, 0, pos.z + 0.3), Quaternion.identity);
		Instantiate(laser, Vector3(pos.x +0.25, 0, pos.z + 0.3), Quaternion.identity);
		AudioSource.PlayClipAtPoint(sndLaser, transform.position);
	}	
}
